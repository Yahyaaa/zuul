package org.zain.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.zain.zuul.filter.AccessLogFilter;

@EnableZuulProxy
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
public class SpringCloudZuulApplication {

	public static void main(String[] args) {
	    SpringApplication.run(SpringCloudZuulApplication.class, args);
	  }

	  @Bean
	  public AccessLogFilter simpleFilter() {
	    return new AccessLogFilter();
	  }

}
